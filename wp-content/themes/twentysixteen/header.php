<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://use.fontawesome.com/d5696bb111.js"></script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>



<header id="navbarSpot">

	<div class="navigation-content">
        <div class="navigation-first">
            <div class="wrapper clearfix">
                <div class="block">
                    <a href="#" title="SPOT-A-SHOP"><span>SPOT-A-SHOP</span></a>
                </div>
                <div class="navigation-main-switch block">
                    <a class="active" href="#" style="margin-left: 10px;">
                        Femmes
                        <div class="current-arrow"></div>                    
                    </a>
                    <a class="" href="#">
                        Hommes
                                            </a>
                    <a class="" href="#">
                        Enfants
                                            </a>
                </div>
                <div class="navigation-search block mobile-hide">
                    <form action="#" class="" autocomplete="off" id="main-search-form">
                        <input type="hidden" name="s" value="femmes">                        <input name="q" id="main-search" type="text" value="" autocomplete="off" placeholder="Rechercher une marque, une catégorie..." class="ac_input">
						<button type="submit" class="btn btn-default">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
                <div class="navigation-main-switch block" >
					<a href="#">
					<i class="fa fa-heart" style="margin-right:5px;margin-left: 10px;"></i>
                        Favoris                
                    </a>
                    <a href="#">
                        Newsletter                  
                    </a>
                    <a href="connexion/">
                        Se connecter                 
                    </a>
                </div>
            </div>
        </div>
	</div>

    <ul id="secondNav">
	  <li><a class="active" href="#home">Vêtement</a></li>
	  <li><a href="#news">Chaussure</a></li>
	  <li><a href="#contact">Accessoire</a></li>
	  <li><a href="#about">Sport</a></li>
  	  <li><a href="#about">Marques</a></li>
  	  <li><a href="#about">Boutiques</a></li>  	  
  	  <li><a href="#about">Inspirations</a></li>  	    	  
	</ul>

		</header><!-- .site-header -->

		<div id="content" class="site-content">
