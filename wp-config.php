<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'spotashop');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Hp^ncz`bbRcR!>A1[rmq`au<d?Yh[n)FtNG<`uG*zS*7=AZq^1~ZOp58XGBxzs8m');
define('SECURE_AUTH_KEY',  '+a&]a85QQ|hV5csof!:)8?;H4ejP(1T*y:%Ze,V-yDXwu$K=+8mMf]K)+=*JM}tQ');
define('LOGGED_IN_KEY',    '?1E_`zHbN?~wxM]4sc1C3oiLzf}}1|$xfeIHZVL?lwv1um9jR()=1%6YlC;Mb_#%');
define('NONCE_KEY',        'WoewkrE6{}.:E>8ELduRtfY1V//QtZ5gSo,&ex^ !x[^Sz}T%Jx%-(]-sKQC$-67');
define('AUTH_SALT',        'lK4 =^,@#H-j]vmKA.xhO,ED@R%NaeJF`0%Vr!`hfuXI:T+eqP9MxHoN0mjh,EBI');
define('SECURE_AUTH_SALT', 'i$E$}_<,4MDOKu4yEH&dNSC_r=ArnGj;1A2T&)pYJyP+(/}f1dyfB~^DnlGkw(xD');
define('LOGGED_IN_SALT',   '.oaphyC}YSyKF:f&tg- ::hMS0n&AI?6<FiAD<{roID}e:*Jhz 7?&cm}%3I8cUa');
define('NONCE_SALT',       '*a8XX!O2LeiR=POMhvFji;J.xK0e6/0^]I&@Ik7HHHg%X3:%@aqNS$Obrn0o[!6<');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d'information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');